#!/usr/bin/env node
var app = require('./app');


app.set('port', process.env.PORT || 8080);
	var server = app.listen(app.get('port'), function() {
	console.log('[%s] Listening on http://localhost:%d', app.settings.env, process.env.PORT || 8080);
});
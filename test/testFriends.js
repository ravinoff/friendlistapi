var should = require('chai').should(),
    supertest = require('supertest'),
    api = supertest('http://localhost:8080'),
    app = require('../app');
describe('FriendsAPI tests:', function() {
	before (function (done) {
		app.listen(8080, function (err, result) {
			if (err) {
				done(err);
			} else {
				db.flushdb();
				done();
			}
		});
	}); 
	it('Create friendshi between UID:1000 and UID: 2000', function(done) {
    api.post('/friends/')
    .send({ "user_id": 1000, "friend_id": 2000})
    .set('Accept', 'application/json')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });

    it('Friendship should be mutal', function(done) {
    api.get('/friends/1000')
    .expect(200)
    .expect('Content-Type', /json/)
    .expect({
    	"user_id": "1000",
    	"friends": ["2000"]})
    .end(function(err, res) {
      if (err) return done(err);
    });
    api.get('/friends/2000')
    .expect(200)
    .expect('Content-Type', /json/)
    .expect({
    	"user_id": "2000",
    	"friends": ["1000"]})
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });

  it('GET returns friend list as JSON for valid UID', function(done) {
    api.get('/friends/1')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      res.body.should.have.property('friends').and.be.instanceof(Array);
      done();
    });
  });

  it('GET returns error for requesting invalid negative UID', function(done) {
    api.get('/friends/-3')
    .expect(400)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('GET should return error for non-integer string UID', function(done) {
    api.get('/friends/Bob')
    .expect(400)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('Adding friend with wrong UID should return error', function(done) {
    api.post('/friends/')
    .send({ "user_id": 1000, "friend_id": "Bob"})
    .set('Accept', 'application/json')
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('Adding valid friendID to invalid UID should fail', function(done) {
    api.post('/friends/')
    .send({ "user_id": "Alice", "friend_id": "1000"})
    .set('Accept', 'application/json')
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('Both friend and user ID invalid should fail as well', function(done) {
    api.post('/friends/')
    .send({ "user_id": "Alice", "friend_id": "Peter"})
    .set('Accept', 'application/json')
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('User cannot add himself as a friend', function(done) {
    api.post('/friends/')
    .send({ "user_id": "1000", "friend_id": "1000"})
    .set('Accept', 'application/json')
    .expect(400)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('Removing non existing relation does nothing', function(done) {
    api.delete('/friends/')
    .send({ "user_id": "5000", "friend_id": "6000"})
    .set('Accept', 'application/json')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('Remove relation between UID:1000 and UID:2000', function(done) {
    api.delete('/friends/')
    .send({ "user_id": "1000", "friend_id": "2000"})
    .set('Accept', 'application/json')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('Friendship should be broken for both UID:1000 and UID:2000', function(done) {
    api.get('/friends/1000')
    .expect(200)
    .expect('Content-Type', /json/)
    .expect({
    	"user_id": "1000",
    	"friends": []})
    .end(function(err, res) {
      if (err) return done(err);
    });
    api.get('/friends/2000')
    .expect(200)
    .expect('Content-Type', /json/)
    .expect({
    	"user_id": "2000",
    	"friends": []})
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
  it('Having multiple friends works', function(done) {
    api.post('/friends/')
    .send({ "user_id": 0, "friend_id": 100000})
    .set('Accept', 'application/json')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
    });
    api.post('/friends/')
    .send({ "user_id": 0, "friend_id": 200000})
    .set('Accept', 'application/json')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
    });
    api.post('/friends/')
    .send({ "user_id": 0, "friend_id": 500000})
    .set('Accept', 'application/json')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      if (err) return done(err);
    });
     api.get('/friends/0')
    .expect(200)
    .expect('Content-Type', /json/)
    .expect({
    	"user_id": 0,
    	"friends": [100000, 200000, 500000]})
    .end(function(err, res) {
      if (err) return done(err);
      done();
    });
  });
});

# FriendlistAPI #

A relation management API written in node.js, express and Redis as data store.
Demo version deployment can be found here: http://friendlistapi-winiarski.rhcloud.com/api/
## Setup ##
Install dependencies 
```
#!bash
npm install

```
Edit config.json with valid Redis instance IP address, port and database number
```
#!json
{
  "development": {
    "redisPort": 6379,
    "redisHost": "127.0.0.1",
    "redisDB" : 0  
    },
  "production": {
    "redisPort": 6379,
    "redisHost": "127.0.0.1",
    "redisDB" : 1
  }
}

```
To run tests:

```
#!bash

npm test
```
Note: Development database is flushed before tests are run.

To start API:

```
#!bash

npm start
```

# API Methods #

## Add friends to DB ###


```
#!ReST

POST /api/friends
```
## Parameters ##

Name     | Type   | Description |
---------:| :----- |:-----:
`user_id`  |  `Integer` | **Required** ID of first user|
`friend_id`     |    `Integer` | **Required** ID of friend, must be different than user_id|

Note: Relation is mutual, so with one call we create relations (UID1; UID2) and (UID2; UID1)

## Example ##

```
#!json

{
	"user_id": 1000,
	"friend_id": 2000
}
```
## Response ##
```
#!json

{
    "status": "SUCCESS"
}
```

```
#!http

Status: 200 OK
Content-Type: application/json
Date: Wed, 17 Dec 2014 18:15:34 GMT
Connection: keep-alive
```

## Remove friendship relation ###


```
#!ReST

DELETE /api/friends
```
## Parameters ##

Name     | Type   | Description |
---------:| :----- |:-----:
`user_id`  |  `Integer` | **Required** ID of first user|
`friend_id`     |    `Integer` | **Required** ID of friend, must be different than user_id|

Note: Relation is mutual, so both (UID1; UID2) and (UID2; UID1) are dropped from database.

## Example ##

```
#!json

{
	"user_id": 1000,
	"friend_id": 2000
}
```
## Response ##
```
#!json

{
    "status": "SUCCESS"
}
```

```
#!http

Status: 200 OK
Content-Type: application/json
Date: Wed, 17 Dec 2014 18:20:16 GMT
Connection: keep-alive
```

## List friends for certain user ###


```
#!ReST

GET /api/friends/:user_id
```
## Parameters ##

Name     | Type   | Description |
---------:| :----- |:-----:
`user_id`  |  `Integer` | **Required** ID of user|

## Response ##
```
#!json

{
    "user_id": "1000",
    "friends": [ "2000", "54354", "1111"]
}
```

```
#!http

Status: 200 OK
Content-Type: application/json
Date: Wed, 17 Dec 2014 18:23:00 GMT
Connection: keep-alive
```

# Sample client in python #
```
#!python
import requests
import json
import random


headers = {
	'Content-type': 'application/json', 
	'Accept': 'text/plain'
}

for i in range(1,100):
	
	# Add some random relations

	u_id = random.randint(1000,2000)
	f_id = random.randint(1000,2000)
	req = json.dumps({"user_id": u_id, "friend_id": f_id})
	print req
	put = requests.post("http://friendlistapi-winiarski.rhcloud.com/api/friends", data = req, headers=headers)
	if put.status_code <> 200:
		print 'ERROR: ', put.status_code
		
	# Then retrieve them for one of the users
	r = requests.get('http://friendlistapi-winiarski.rhcloud.com/api/friends/1000',headers=headers)
	print r.json()
```
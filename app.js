var util = require('util');
var express = require('express'); 		
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var redis = require("redis");

var app = express(); 				

var config = require('./config.json')[app.get('env')];

// Connect to Redis
global.db = redis.createClient(config.redisPort,config.redisHost);

db.select(config.redisDB, function(err,res){
	//console.log('SELECT Redis DB ' + res + ' ' + config.redisDB)
	});

db.on("error", function (err) {
      //  console.log("Error " + err);
    });
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(expressValidator({
	customValidators: {
    	notNegative: function(value) {
        	return value >= 0;
    		},
    	notEqual: function(param, comp){
    		return param != comp;
    		}
		}}));



var router = express.Router(); 				
var friends = require('./routes/friends');

app.use('/api',friends);

app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

if (app.get('env') === 'development') {
			app.use(function(err, req, res, next) {
			res.status(err.status || 500);
			res.send('error', {
				message: err.message,
				error: err
		});
	});
}

app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.send('error', {
		message: err.message,
		error: {}
	});
});


module.exports = app;

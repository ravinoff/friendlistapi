import requests
import json
import random


headers = {
	'Content-type': 'application/json', 
	'Accept': 'text/plain'
}

for i in range(1,100):
	
	# Add some random relations

	u_id = random.randint(1000,2000)
	f_id = random.randint(1000,2000)
	req = json.dumps({"user_id": u_id, "friend_id": f_id})
	print req
	put = requests.post("http://friendlistapi-winiarski.rhcloud.com/api/friends", data = req, headers=headers)
	if put.status_code <> 200:
		print 'ERROR: ', put.status_code
		
	# Then retrieve them for one of the users
	r = requests.get('http://friendlistapi-winiarski.rhcloud.com/api/friends/1000',headers=headers)
	print r.json()
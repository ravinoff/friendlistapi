var express = require('express');

module.exports = (function() {

	var router = express.Router();
	router.route('/friends')
		.post(function(req,res) {
			req.checkBody('user_id', 'Bad Request').notEmpty().isInt().notNegative();
			req.checkBody('friend_id', 'Bad Request').notEmpty().isInt().notNegative().notEqual(req.body.user_id);

			var err = req.validationErrors();
			//console.log(err)

			if (err) {
	   			res.send(400,{status: "ERROR"});
	    		return;
	  		}
			db.MULTI()
			.sadd(req.body.user_id, req.body.friend_id)
			.sadd(req.body.friend_id, req.body.user_id)
			.exec(function (err,replies){
				if(err){
					res.send({errors: "error"},500)
				}
				//console.log("ERORS: " + err);
				//console.log("MULTI: " + replies.length)
				replies.forEach(function (reply, index) {
	                //console.log("Reply " + index + ": " + reply.toString());
	            });
	            res.json({ status: "SUCCESS" });
			})
		})
		.delete(function(req,res) {
			req.checkBody('user_id', 'Bad Request').notEmpty().isInt().notNegative();
			req.checkBody('friend_id', 'Bad Request').notEmpty().isInt().notNegative().notEqual(req.body.user_id);

			var err = req.validationErrors();

			if (err) {
	   			res.send(400,{status: "ERROR"});
	    		return;
	  		}
			db.MULTI()
			.srem(req.body.user_id, req.body.friend_id)
			.srem(req.body.friend_id, req.body.user_id)
			.exec(function (err,replies){
				if(err){
					res.send({errors: "error"},500)
				}
				//console.log("ERORS: " + err);
				//console.log("MULTI: " + replies.length)
				replies.forEach(function (reply, index) {
	               // console.log("Reply " + index + ": " + reply.toString());
	            });
	            res.json({ status: "SUCCESS" });
			})
		})

	router.route('/friends/:user_id')	
		.get(function(req,res) {
			req.checkParams('user_id', 'Non-integer or negative user_id').notNegative();

			var errors = req.validationErrors();
			//console.log(errors);
			if (errors){
				res.send({error: errors}, 400);
	    		return;
			}
			db.smembers(req.params.user_id, function (err,obj){
				if(err){
					res.send({errors: "error"},500)
				}
				res.json({ user_id: req.params.user_id, friends: obj });
			});
			
		});
	return router;
})();